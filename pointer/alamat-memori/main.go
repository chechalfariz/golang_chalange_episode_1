package main

import "fmt"

func main() {
	var name = "Kenji"
	var age = 25

	fmt.Println("Name :", name)
	fmt.Println("alamat dari name :", &name)

	fmt.Println("age :", age)
	fmt.Println("alamat dari age :", &age)
}
