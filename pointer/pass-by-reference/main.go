package main

import "fmt"

func main() {
	var x int = 4
	var y *int = &x

	fmt.Println("X :", x)
	fmt.Println("Alamat x :", &x)
	fmt.Println("y :", y)
	fmt.Println("Alamat y :", &y)

	fmt.Println("Nilai reference dari pointer y :", *y)
}
