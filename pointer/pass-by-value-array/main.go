package main

import "fmt"

func main() {
	var numbers = [4]int{4, 5, 7, 1}
	var anotherNumbers = numbers

	fmt.Println("Sebelum ============================")
	fmt.Println("Numbers :", numbers)
	fmt.Println("Another Numbers :", anotherNumbers)

	numbers[1] = 9
	fmt.Println("Sesudah ============================")
	fmt.Println("Numbers :", numbers)
	fmt.Println("Another Numbers :", anotherNumbers)

	fmt.Println("=========================================")

	var scores = [4]int{7, 8, 6, 9}
	multiplyBy10(scores)

	fmt.Println("scores :", scores)
}

func multiplyBy10(n [4]int) {
	for i := range n {
		n[i] = n[i] * 10
	}

	fmt.Println("N in function :", n)
}
