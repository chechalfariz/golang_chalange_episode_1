package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Banyak kelereng Si Pemberi")
	scanner.Scan()
	giver, _ := strconv.Atoi(scanner.Text())
	fmt.Println("Banyak kelereng Yang Dimiliki si penerima")
	scanner.Scan()
	receiver, _ := strconv.Atoi(scanner.Text())
	fmt.Println("Banyak kelereng Yang akan dibagikan oleh si Pemberi")
	scanner.Scan()
	marble, _ := strconv.Atoi(scanner.Text())

	giveMarble(&giver, &receiver, marble)
	fmt.Printf("%d Sisa kelereng si penerima \n", giver)
	fmt.Printf("%d banyak kelereng yang dimiliki si penerima", receiver)
}

func giveMarble(giver *int, receiver *int, marble int) {
	if marble > *giver {
		fmt.Println("Kelereng yang diberikan tidak cukup")
	} else {
		*giver -= marble
		*receiver += marble
	}
}
