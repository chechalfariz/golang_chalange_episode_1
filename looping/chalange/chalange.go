package main

import "fmt"

func main() {
	var bla int
	fmt.Scanln(&bla)

	// Solusi Pertama (Sangat Efektif)
	for bla >= 1 {
		fmt.Println(bla, "I will become a go developer")

		bla--
	}

	// Solusi Baru tapi terlalu (tidak efektif)

	// for i := bla; i >= 1; i-- {
	// 	fmt.Println(bla, "I will become a go developer")

	// 	bla--
	// }
}
