package main

import "fmt"

func main() {
	var inputJumlah int
	fmt.Scan(&inputJumlah)
	var inputSekat = make([]int, inputJumlah)
	var potongan int

	// Meminta input dari pengguna tentang jumlah bambu yang dimilikinya
	// Kemudian menyimpan jumlah tersebut dalam variabel inputJumlah
	for i := 0; i < inputJumlah; i++ {
		fmt.Println("Input Sekat ke-", i+1)
		fmt.Scan(&inputSekat[i])
	}

	// Meminta input dari pengguna tentang berapa kali potongan yang ingin dilakukan
	// Kemudian menyimpan jumlah potongan tersebut dalam variabel potongan
	fmt.Println("Berapa Potongan ?")
	fmt.Scan(&potongan)

	// Memanggil fungsi potongBambu untuk setiap potongan yang ingin dilakukan
	for i := 1; i <= potongan; i++ {
		potongBambu(i, inputSekat)
	}
}

// potongBambu adalah fungsi untuk melakukan potongan pada bambu
// potongan adalah nomor potongan yang sedang dilakukan
// inputSekat adalah slice yang berisi jumlah sekat dari setiap bambu
func potongBambu(potongan int, inputSekat []int) {
	// Menampilkan nomor potongan
	fmt.Printf("Potongan ke-%d\n", potongan)
	// Melakukan iterasi pada setiap bambu untuk mengurangi satu sekat dari setiap bambu yang memiliki sekat lebih dari 0
	for i := range inputSekat {
		if inputSekat[i] > 0 {
			inputSekat[i]-- // Mengurangi nilai sekat setiap bambu jika nilainya lebih dari 0
		}
		// Menampilkan jumlah sekat setiap bambu setelah potongan
		fmt.Printf("Bambu ke-%d: %d\n", i+1, inputSekat[i])
	}
}
