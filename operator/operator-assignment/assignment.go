package main

import "fmt"

func main() {
	fmt.Println("Operator Assignment")

	// var a int = 10
	// a = a - 3
	// fmt.Println("nilai a =", a)

	var a int = 5
	var b int
	b = a
	fmt.Println("nilai b =", b)
	b += a
	fmt.Println("nilai b =", b)
	b -= a
	fmt.Println("Nilai b =", b)
	b *= a
	fmt.Println("Nilai b =", b)
	b /= a
	fmt.Println("Nilai b =", b)
	b %= a
	fmt.Println("Nilai b =", b)
}
