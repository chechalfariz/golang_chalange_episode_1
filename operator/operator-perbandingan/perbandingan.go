package main

import "fmt"

func main() {
	fmt.Println("operator perbandingan")

	a := 9
	b := 3
	fmt.Println(a == b)
	fmt.Println(a != b)
	fmt.Println(a < b)
	fmt.Println(a <= b)
	fmt.Println(a > b)
	fmt.Println(a >= b)
}
