package main

import "fmt"

func main() {
	fmt.Println("operator logika")

	a := true
	b := false
	fmt.Println(a && b)
	fmt.Println(a || b)
	fmt.Println(!a)

	fmt.Println(false || true && false)
}
