package main

import "fmt"

type triangle struct {
	base  float64
	heigh float64
}

type counter struct {
	value int
}

func (t triangle) area() float64 {
	return 0.5 * t.base * t.heigh
}

// penambahan size pada struct triangle dengan menggunakan pointer untuk mengambil memori
// dari func sebelumnya pass by reference. dan bisa juga mengembalika return
// pada value reciever

func (t *triangle) increaseSize() {
	t.base += 1
	t.heigh += 1
}

func (c counter) increment() {
	c.value++
}

func main() {
	instanceTriange := triangle{10, 12}
	area := instanceTriange.area()
	fmt.Println("area :", area)

	fmt.Println("==================================")
	fmt.Println("instance Triange:", instanceTriange)
	instanceTriange.increaseSize()
	fmt.Println("instance Triange:", instanceTriange)

	fmt.Println("====================================")
	counter1 := &counter{value: 3}
	counter1.increment()
	fmt.Println("value setelah di-increment:", counter1.value)
}
