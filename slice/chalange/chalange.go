package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func evenNames(slice []string) string {
	//Tulis kode disini
	var evenStr []string
	for _, str := range slice {
		if len(str)%2 == 0 {
			evenStr = append(evenStr, str)
		}
	}
	return strings.Join(evenStr, " ")
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	x := scanner.Text()
	slice := strings.Split(x, " ")
	names := evenNames(slice)
	fmt.Println(names)
}
