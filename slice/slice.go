package main

import "fmt"

func main() {
	numbersSlice := []int{2, 7, 9, 4}

	fmt.Println(numbersSlice)

	fmt.Println("Panjang Slice :", len(numbersSlice))
	fmt.Println("Kapasitas Slice :", cap(numbersSlice))

	fmt.Println("======================================")

	scores := make([]int, 4, 7)
	scores[0] = 70
	scores[1] = 80
	scores[2] = 95
	scores[3] = 65
	fmt.Println(scores)
	fmt.Println("Panjang Scores :", len(scores))
	fmt.Println("Kapasitas Scores :", cap(scores))

	fmt.Println("======================================")

	scores2 := make([]int, 4)
	scores2[0] = 70
	scores2[1] = 80
	scores2[2] = 95
	scores2[3] = 65

	fmt.Println(scores2)
	fmt.Println("Panjang Scores 2:", len(scores2))
	fmt.Println("Kapasitas Scores 2:", cap(scores2))

	fmt.Println("======================================")

	heroes := [4]string{"Superman", "Batman", "Spiderman", "Iron Man"}

	fmt.Println("Heroes: ", heroes)
	//heroes[5] = "Catwomen" bakalan eror jika dipaksa tambahkan index tanpa slice

	villain := make([]string, 3, 5)
	villain[0] = "Thanos"
	villain[1] = "Joker"
	villain[2] = "Homelander"
	villain = append(villain, "Ultron")
	villain = append(villain, "Sauron", "voldemort")
	fmt.Println("Villain : ", villain)
	fmt.Println("Panjang Villain :", len(villain))
	fmt.Println("Kapasitas Villain: ", cap(villain))

	fmt.Println("======================================")

	var numbers = [4]int{2, 1, 6, 8}
	var anotherNumbers = numbers
	fmt.Println("Numbers :", numbers)
	fmt.Println("Another Numbers :", anotherNumbers)
	anotherNumbers[1] = 11
	fmt.Println("Numbers :", numbers)
	fmt.Println("Another Numbers :", anotherNumbers)

	fmt.Println("======================================")

	var prices = []int{2000, 13000, 10000}
	var anotherPrices = prices
	fmt.Println("Prices :", prices)
	fmt.Println("Another Prices :", anotherPrices)
	anotherPrices[1] = 15000
	fmt.Println("Prices :", prices)
	fmt.Println("Another Prices :", anotherPrices)

	fmt.Println("======================================")

	ages := [4]int{20, 22, 25, 19}
	sliceAges := ages[0:3]
	fmt.Println("Ages :", ages)
	fmt.Println("SliceAges :", sliceAges)
	sliceAges[2] = 33
	fmt.Println("Ages :", ages)
	fmt.Println("SliceAges :", sliceAges)
	sliceAges = append(sliceAges, 37)
	fmt.Println("Ages :", ages)
	fmt.Println("SliceAges :", sliceAges)

}
