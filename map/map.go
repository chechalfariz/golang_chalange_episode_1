package main

import "fmt"

func main() {
	user := map[string]string{
		"username": "kenjif",
		"email":    "kenji@mail.com",
	}
	fmt.Println(user)

	fmt.Println("====================================")

	var scores = make(map[string]int)
	fmt.Println(scores)

	scores["java"] = 85
	scores["react"] = 87
	scores["kotlin"] = 90

	fmt.Println("Scores :", scores)

	fmt.Println("Nilai Java :", scores["java"])
	fmt.Println("Nilai Kotlin :", scores["kotlin"])
	fmt.Println("Nilai Golang :", scores["golang"])

	scores["java"] = 90
	fmt.Println("====================================")

	fmt.Println("Nilai Java :", scores["java"])

	fmt.Println("====================================")

	delete(scores, "kotlin")

	fmt.Println("Scores :", scores)

	fmt.Println("====================================")

	names := map[int]string{
		1: "john",
		2: "jane",
		3: "lili",
		4: "rubi",
	}

	for _, value := range names {
		fmt.Println("Value :", value)
	}
}
