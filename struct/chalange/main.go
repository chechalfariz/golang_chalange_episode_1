package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

// Tulis kode struct disini
type Penduduk struct {
	nama   string
	umur   int
	alamat string
}

type Student struct {
	name string
	age  int
}

func main() {
	// ini chalange 1
	/*
		var penduduk1 Penduduk
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		penduduk1.nama = scanner.Text()
		scanner.Scan()
		penduduk1.umur, _ = strconv.Atoi(scanner.Text())
		scanner.Scan()
		penduduk1.alamat = scanner.Text()

		//Tulis kode format disini

		// ini best Practice
		fmt.Printf("Hello, my name is %s. Im %d years old. I live in %s", penduduk1.nama, penduduk1.umur, penduduk1.alamat)

		// ini asal jadi
		fmt.Print("Hello, my name is ", penduduk1.nama, ". ", "Im ", penduduk1.umur, "years old.", " I live in ", penduduk1.alamat)
	*/

	fmt.Println("============================================")

	// Menggunakan Bufio input console

	var student1 Student
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	student1.name = scanner.Text()
	scanner.Scan()
	student1.age, _ = strconv.Atoi(scanner.Text())

	student1.Introduction()

	// statis tanpa input dari console
	student2 := Student{
		name: "Sammy",
		age:  17,
	}

	student2.Introduction()

}
func (s *Student) Introduction() {
	fmt.Printf("Hello, my name is %s. Im %d years old", s.name, s.age)
}
