package main

import "fmt"

type kendaraan struct {
	merek string
	tahun int
	model string
	harga int
	mesin
}

type mesin struct {
	tipe string
	cc   int
}

func main() {
	// terjadi eror ketika ada embded struct atau pivot contoh seperti mesin di pivot
	// kedalam kendaraan
	/*
		var a kendaraan
		a.merek = "honda"
		a.tahun = 2019
		a.model = "inova"
		a.harga = 700000000

		fmt.Println("a", a)
		fmt.Println("merek :", a.merek)
		fmt.Println("tahun :", a.tahun)
		fmt.Println("model :", a.model)
		fmt.Println("harga :", a.harga)

		fmt.Println("====================================")

		var b = kendaraan{}
		b.merek = "Wuling"
		b.tahun = 2029
		b.model = "INtam"
		b.harga = 45000000

		fmt.Println("b :", b)

		// mengisi dengan langsung dari kurawal
		var c = kendaraan{"Yamaha", 2021, "mio", 20000000}

		fmt.Println("c :", c)

		fmt.Println("====================================")

		// mengisi dengan acak tidak sesuai urutan
		var d = kendaraan{merek: "Yamaha", tahun: 2023, harga: 2000000, model: "aerox"}

		fmt.Println("D : ", d)

		fmt.Println("====================================")

		var x = kendaraan{merek: "honda", tahun: 2021, model: "hrv", harga: 2300000}
		var y kendaraan = x

		fmt.Println("x:", x)
		fmt.Println("y:", y)
		fmt.Printf("x alamat : %p\n", &x)
		fmt.Printf("y alamat : %p\n", &y)
		y.model = "PCX"
		fmt.Println("setelah diubah")
		fmt.Println("X: ", x)
		fmt.Println("Y: ", y)

		// fmt.Println("=============================")
		// var z = kendaraan{merek: "Lambo", model: "Lamborr", harga: 350000000, tahun: 2013}

		// // Mengupdate variable z dengan function
		// updateKendaraan(z)

		// // isi yang sebenarnya di variable z
		// fmt.Println("Kendaraan di function main", z)

		// fmt.Println("=============================")

		// var z2 *kendaraan = &z
		// fmt.Printf("alamat z : %p \n", &z)
		// fmt.Printf("alamat z2 : %p \n", z2)
		// z2.model = "CRV"
		// fmt.Println("z: ", z)
		// fmt.Println("z2: ", z2)

		fmt.Println("=============================")

		var z = kendaraan{merek: "Honda", tahun: 2023, model: "civic", harga: 70000000}
		updateKendaraan(&z)
		fmt.Println("kendaraan di function main :", &z)
	*/

	// contoh embeded struct atau pivot struct
	var a = kendaraan{
		merek: "Toyota",
		tahun: 2018,
		model: "camry",
		harga: 25000000,
		mesin: mesin{
			tipe: "premium",
			cc:   2000,
		},
	}

	fmt.Println("A :", a)
	fmt.Println("A mesin.Tipe", a.mesin.tipe)

}

func updateKendaraan(newKendaraan *kendaraan) {
	newKendaraan.merek = "Toyota"
	newKendaraan.model = "Camry"
	newKendaraan.harga = 65000000
	newKendaraan.tahun = 2023
	fmt.Println("kendaraan di function updateKendarraan: ", newKendaraan)
}
