package main

import (
	"fmt"
)

func main() {
	// use defer for end execution. example String start will be execute if func min done

	/*
		defer fmt.Println("start")
		fmt.Println("Proccesing")
		fmt.Println("Done")
	*/

	/*
		num1 := 4
		num2 := 0
	*/

	/*
		fmt.Println("start")
		fmt.Println("")

		if num2 == 0 || num1 == 0 {
			fmt.Println(errors.New("pembagian dengan angka 0 "))
			return
		}

		numResult := num1 / num2

		fmt.Println("Hasil", numResult)
	*/

	/*
		defer fmt.Println("Done") // will hold after func mine done compile, and still be print because the "Done" already compile before panic error num1/num2
		fmt.Println("Start")
		fmt.Println(num1 / num2)
	*/

	// LIFO (last in first out), defer last will be out first
	/*
		fmt.Println("Start")
		defer fmt.Println("Done")
		defer add(1, 2)
		defer add(3, 4)
		defer multiply(3, 3)
	*/

	// anoter LIFO example

	// first execute
	fmt.Println("Start")
	// thrid execute, this will print func loop
	defer loop()
	// second execute
	fmt.Println("Done")

	for i := 0; i < 5; i++ {
		defer fmt.Print(i, "")
	}
}

func add(num1 int, num2 int) {
	result := num1 + num2
	fmt.Println(result)
}

func multiply(num1 int, num2 int) int {
	result := num1 * num2
	fmt.Println(result)
	return result
}

func loop() {
	for i := 1; i <= 5; i++ {
		// because using defer, loop start with 5 until 1
		defer fmt.Println(i)
	}

	// first print
	fmt.Println("Done Loop")
}
