package main

import "fmt"

func main() {

	// this func is annonymouse function
	// this function before prgramm stopped from panic. this function will execute
	defer func() {
		// r is recover function when panic will execute this condition
		if r := recover(); r != nil {
			fmt.Println("Terjadi Panic : ", r)
		}
	}()

	var input string
	fmt.Print("Masukan nama : ")
	fmt.Scanln(&input)

	fmt.Println("Start")

	// if func isEmpty false will return sukkses, but if func isEmpty not have value then will call panic function and sukkses will not execute or program will stopped
	if !isEmpty(input) {
		fmt.Println("nama Kamu adalah : ", input)
	}
	fmt.Println("Sukkses")
}

// this func have parameter string, and name return value boolean
func isEmpty(input string) (empty bool) {
	if input == "" {
		panic("Input Tidak Boleh Kosong !!!")
	}
	empty = false
	return
}
