package main

import "fmt"

type Patient struct {
	Name string
	Age  int
	Celsius
}

type Celsius float64
type Temperature int

func (c Celsius) toFarenheit() float64 {
	return float64(c*9/5 + 32)
}

func (c *Celsius) add(value float64) {
	*c += Celsius(value)
}
func (t Temperature) Display() {
	fmt.Printf("suhu: %d derajat Celsius\n", t)
}

func main() {
	var temperature Celsius = 20.0

	fmt.Println("Temperature :", temperature)
	fmt.Println("suhu di ruangan ini,", temperature.toFarenheit(), "derajat Farenheit")
	temperature.add(3)
	fmt.Println("Suhu diruangan ini menjadi", temperature, "derajat celsius")

	fmt.Println("=====================================================")

	newPatient := Patient{Name: "kenji", Age: 25, Celsius: 39.0}
	fmt.Printf("newPatient: %+v \n", newPatient)

	fmt.Println("=====================================================")

	temp := Temperature(25)
	temp.Display()
}
