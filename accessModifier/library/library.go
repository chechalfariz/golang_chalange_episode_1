package library

const secondsInMinute int = 60
const minutesInHour int = 60
const HourInADay int = 24

func daysToHour(day int) int {
	return day * HourInADay
}

/*
jika variable unexported tidak bisa di gunakan di luar package library
maka caranya menggunakan function dengan memanggil variable tersebut dibungkus didalm function
*/

func DaysToMinutes(day int) int {
	return day * HourInADay * minutesInHour
}
