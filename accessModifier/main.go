package main

import (
	"accessModifier/library"
	"fmt"
)

func main() {
	fmt.Println("HourInADay :", library.HourInADay)
	/*
		kerna memaksakan huruf awalnya adlaah huruf besar jadi eror dan tidak bisa di export

		fmt.Println("secondsInMinute Unexported:", library.secondsInMinute)

	*/

	/*
		mengkases variable export dari beda file namun tetap 1 package
	*/
	fmt.Println("Name Library2.go :", library.Name)

	/*
		mengambil function dari package library
	*/
	fmt.Println(library.DaysToMinutes(3))
}
