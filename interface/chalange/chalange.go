package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

type BangunDatar interface {
	Luas() int
	Keliling() int
}

type Segitiga struct {
	Alas   int
	Tinggi int
}

func (s *Segitiga) Luas() int {
	return (s.Alas * s.Tinggi) / 2
}

func (s *Segitiga) Keliling() int {
	return 0
}

func getLuas(bangunDatar BangunDatar) {
	fmt.Printf("Luas bangun datar : %d /n", bangunDatar.Luas())
}

func main() {
	var segitiga1 Segitiga
	scanner := bufio.NewScanner(os.Stdin)

	scanner.Scan()
	segitiga1.Alas, _ = strconv.Atoi(scanner.Text())
	scanner.Scan()
	segitiga1.Tinggi, _ = strconv.Atoi(scanner.Text())

	getLuas(&segitiga1)
}
