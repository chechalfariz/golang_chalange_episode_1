package main

import "fmt"

func main() {
	num := []int{4, 1, 3, 7, 4, 5}
	kecil, besar, rata2 := minMax(num)

	fmt.Println("Kecil :", kecil)
	fmt.Println("Besar :", besar)
	fmt.Println("Rata Rata :", rata2)
}

func minMax(numbers []int) (min int, max int, avg int) {
	min = numbers[0]
	max = numbers[0]
	avg = numbers[0]

	for _, n := range numbers {
		if n < min {
			min = n
		}
		if n > max {
			max = n
		}
		for i := n + 1; i < len(numbers); i++ {
			if n == numbers[i] {
				avg = n
				break
			}
		}
	}
	return
}
