package main

import "fmt"

var name = "Kenji"

func main() {
	helloWorld()
	fmt.Println("main:", name)

	greet("kenjji", 25)
	greet("John", 19)
	greet("Doe", 30)

	add(2, 7)
	// total := add(a, 7)
}

func helloWorld() {
	var name = "ferdian"
	fmt.Println("hello world!")
	fmt.Println("Hello rolrd:", name)
}

func greet(name string, age int) {
	fmt.Println("Hello my name is :", name, "aim", age, "years old")
}

func add(a int, b int) {
	fmt.Println("Result add", a+b)
}
