package main

import "fmt"

func main() {
	//fmt.Println("hasil", add(4, 10000))

	total := add(5, 2)
	fmt.Println("Hasil Penjumlahan :", total)

	// total2 := multiply(3, 8)
	// fmt.Println("Hasil perkalian :", total2)
	fmt.Println("Hasil Perkalian :", multiply(3, 8))

	// calculation := add(7, multiply(4, 2))
	// fmt.Println("Hasil Kalkulasi", calculation)

	fmt.Println("Hasil kalkulasi tambah dan kali", add(7, multiply(4, 2)))
}

func add(a int, b int) int {
	result := a + b
	return result
}

// parameter yang sama hanya memangil 1 tipe datanya saja jika sama
func multiply(a, b int) int {
	result := a * b
	return result
}
