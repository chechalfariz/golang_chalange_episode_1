package main

import (
	"fmt"
)

func main() {
	// Anonymous Function
	func() {
		fmt.Println("Hello WOrld")
	}()

	// Anonymous FUnciton dalam variable
	halo := func() {
		fmt.Println("Hallo DUnia")
	}

	halo()

	// Passing Argument kedalam anonymous function
	func(word string) {
		fmt.Println(word)
	}("Enigma Camp")

	// passing argument dalam variable function
	hello := func(name string) {
		fmt.Println("Hello, ", name)
	}

	hello("Jonny")

	// passing anonymous function sebagai argument
	greetEnglish := func(name string) string {
		return "hello " + name
	}
	greetRussian := func(name string) string {
		return "sukblyawat " + name
	}

	greetKorean := func(name string) string {
		return "anyeong " + name
	}

	greet("Rick", greetEnglish)
	greet("Rick", greetRussian)
	greet("Rick", greetKorean)

	add := func(num1 int, num2 int) int {
		return num1 + num2
	}
	multiply := func(num1 int, num2 int) int {
		return num1 * num2
	}
	calculate(3, 2, add)
	calculate(4, 5, multiply)
}

func greet(name string, f func(name string) string) {
	fmt.Println(f(name))
}

func calculate(a int, b int, operator func(x int, y int) int) {
	fmt.Println(operator(a, b))
}
